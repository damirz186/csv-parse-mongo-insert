# csv-parse-mongo-insert

Sample App built as a part of HCS Assignment, it parses provided CSV, and inserts data in MongoDB

Prerequisites
- node v16 (if you use nvm, .nvmrc is provided for auto switching)
- yarn (this one is optional, you should be able to run it with npm)

Before starting app run ```yarn``` or ```npm install```

Starting app
```yarn start```
or
```npm start```

Start script can accept parameters run ```yarn start -h``` or ```npm start -h``` to check

Testing app
```yarn test```
or
```npm test```

Tech stack reasoning
- NodeJS - well, that was a assignment requirement
- csvtojson - https://github.com/Keyang/node-csvtojson csv to json parser that fits the requirement perfectly
- mongodb - official mongodb npm package for manipulating with mongodb
- jest - battle tested automation / testing framework
- yargs - parsing argument strings with ease
- shelf/jest-mongodb - jest preset to run in memory MongoDB for testing, in order to not write / delete actual mongodb instance
- rewire - tool to use non exported functions in jest (or any other test framework)
- dotenv - probably not needed here, as there are only 2 variables, but is generaly useful


