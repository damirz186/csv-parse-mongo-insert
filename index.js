/* eslint-disable no-prototype-builtins */
/* eslint-disable no-param-reassign */
/* eslint-disable no-console */
require('dotenv').config();
const { MongoClient } = require('mongodb');
const csv = require('csvtojson');
const yargs = require('yargs');

// Since no models are used, this object is used to remap keys of parsed csv objects
const remapObj = {
  'Program Identifier': 'programIdentifier',
  'Data Source': 'dataSource',
  'Card Number': 'cardNumber',
  'Member ID': 'memberId',
  'First Name': 'firstName',
  'Last Name': 'lastName',
  'Date of Birth': 'dateOfBirth',
  'Address 1': 'address1',
  'Address 2': 'address2',
  City: 'city',
  State: 'state',
  'Zip code': 'zipCode',
  'Telephone number': 'telephoneNumber',
  'Email Address': 'emailAddress',
  CONSENT: 'consent',
  'Mobile Phone': 'mobilePhone',
};

/**
 * Parses provided csv file using provided delimiter
 *
 * @param {string} file - csv file to parse
 * @param {string} delimiter - delimiter to use when parsing csv
 * @returns {Promise}
 */

const parseCsvFile = async (file, delimiter) => csv({ delimiter })
  .fromFile(file)
  .subscribe((jsonObj) => {
    Object.keys(jsonObj).forEach((key) => {
      if (jsonObj.hasOwnProperty(key) && key in remapObj) {
        jsonObj[remapObj[key]] = jsonObj[key];
        delete jsonObj[key];
      }
    });
  });

/**
 * Inserts data in provided mongo collection
 *
 * @param {object} collection - mongo db collection connection
 * @param {array} data - data to insert in mongo collection
 * @returns {Promise}
 */

const insertDataInMongo = async (collection, data) => collection.insertMany(data);

/**
 * Finds suitable patients for email scheduling
 *
 * @param {array} patients - array of all patients
 * @returns {array}
 */

const findPatientsForEmailScheduling = (patients) => patients.filter((patient) => patient.consent === 'Y' && patient.emailAddress && patient.emailAddress.length > 0);

/**
 * Creates array of emails for inserting in mongo per given specification
 *
 * @param {array} patients - array of patients to create emails for
 * @returns {array}
 */

const createEmailCollection = (patients) => {
  const emails = [];

  patients.forEach((patient) => {
    const now = new Date();
    const newDate = new Date();
    for (let i = 0; i < 4; i++) {
      emails.push(
        {
          id: `${i + 1} ${patient.emailAddress}`, // use composite key for id day + email with whitespace in between
          Name: `Day ${i + 1}`,
          scheduled_time: newDate.setDate(now.getDate() + i + 1),
        },
      );
    }
  });
  return emails;
};

/**
 * Main function used to parse command line arguments and executes assignment
 *
 * @returns {number} - 0 for successfull execution, -1 for failed execution
 */

const main = async () => {
  const { argv } = yargs
    .option('file', {
      description: 'File to parse',
      alias: 'y',
      type: 'string',
    })
    .option('delimiter', {
      alias: 'd',
      description: 'CSV delimiter',
      type: 'string',
    })
    .help()
    .alias('help', 'h');

  let fileToRead = 'data.csv'; // sample file
  let delimiter = '|';
  if (argv.file) {
    fileToRead = argv.file;
    console.info(`Using csv file at location ${fileToRead}`);
  } else {
    console.info('No csv file passed, using a sample data');
  }
  if (argv.delimiter) {
    delimiter = argv.delimiter;
    console.info(`Using provided delimiter ${delimiter}`);
  } else {
    console.info('No delimiter passed, using default delimiter |');
  }
  const patients = await parseCsvFile(fileToRead, delimiter);
  if (patients.length === 0) {
    console.error('No valid data to insert in DB');
    return -1;
  }
  (async () => {
    const client = await MongoClient.connect(process.env.MONGO_URI);

    const db = client.db(process.env.MONGO_DB);
    try {
      const patientsCollections = await db.collection('Patients');
      await insertDataInMongo(patientsCollections, patients);
      console.info('Patients successfully created');
      const emails = createEmailCollection(findPatientsForEmailScheduling(patients));
      const emailsCollection = await db.collection('Emails');
      await insertDataInMongo(emailsCollection, emails);
      console.info('Emails successfully created');
    } catch (err) {
      console.error(err);
    } finally {
      client.close();
    }
  })()
    .catch((err) => {
      console.error(err);
      return -1;
    });
  return 0;
};

module.exports.main = main;
