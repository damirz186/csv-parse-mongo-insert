const { MongoClient } = require('mongodb');
const rewire = require('rewire');
const csv = require('csvtojson');

const indexModule = rewire('./index.js');

describe('Test cases required by assignment', () => {
  let connection;
  let db;
  let patientData;
  let preparedEmails;
  const sampleData = 'data.csv';
  const delimiter = '|';
  let samplePatients;

  beforeAll(async () => {
    connection = await MongoClient.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
    });
    db = await connection.db(global.__MONGO_DB_NAME__);
    samplePatients = await csv({ delimiter }).fromFile(sampleData);
  });

  afterAll(async () => {
    connection.close();
  });

  it('should read and parse all records from csv', async () => {
    const parseCsv = indexModule.__get__('parseCsvFile');
    patientData = await parseCsv(sampleData, delimiter);

    expect(patientData.length).toEqual(samplePatients.length);
    for (let i = 0; i < samplePatients.length; i++) {
      expect(samplePatients[i]['Program Identifier']).toEqual(patientData[i].programIdentifier);
      expect(samplePatients[i]['Data Source']).toEqual(patientData[i].dataSource);
      expect(samplePatients[i]['Card Number']).toEqual(patientData[i].cardNumber);
      expect(samplePatients[i]['Member ID']).toEqual(patientData[i].memberId);
      expect(samplePatients[i]['First Name']).toEqual(patientData[i].firstName);
      expect(samplePatients[i]['Last Name']).toEqual(patientData[i].lastName);
      expect(samplePatients[i]['Date of Birth']).toEqual(patientData[i].dateOfBirth);
      expect(samplePatients[i]['Address 1']).toEqual(patientData[i].address1);
      expect(samplePatients[i]['Address 2']).toEqual(patientData[i].address2);
      expect(samplePatients[i].City).toEqual(patientData[i].city);
      expect(samplePatients[i].State).toEqual(patientData[i].state);
      expect(samplePatients[i]['Zip code']).toEqual(patientData[i].zipCode);
      expect(samplePatients[i]['Telephone number']).toEqual(patientData[i].telephoneNumber);
      expect(samplePatients[i]['Email Address']).toEqual(patientData[i].emailAddress);
      expect(samplePatients[i].CONSENT).toEqual(patientData[i].consent);
      expect(samplePatients[i]['Mobile Phone']).toEqual(patientData[i].mobilePhone);
    }
  });

  it('should insert parsed data correctly to mongo', async () => {
    const patients = db.collection('Patients');
    const insertDataInMongo = indexModule.__get__('insertDataInMongo');
    await insertDataInMongo(patients, patientData);
    // eslint-disable-next-line max-len
    const insertedPatients = await patients.find().project({ _id: 0 }).toArray(); // to fetch all patients from mongo without _id elem and try to match them with existing data
    expect(insertedPatients.length).toEqual(samplePatients.length);
    for (let i = 0; i < samplePatients.length; i++) {
      expect(samplePatients[i]['Program Identifier']).toEqual(insertedPatients[i].programIdentifier);
      expect(samplePatients[i]['Data Source']).toEqual(insertedPatients[i].dataSource);
      expect(samplePatients[i]['Card Number']).toEqual(insertedPatients[i].cardNumber);
      expect(samplePatients[i]['Member ID']).toEqual(insertedPatients[i].memberId);
      expect(samplePatients[i]['First Name']).toEqual(insertedPatients[i].firstName);
      expect(samplePatients[i]['Last Name']).toEqual(insertedPatients[i].lastName);
      expect(samplePatients[i]['Date of Birth']).toEqual(insertedPatients[i].dateOfBirth);
      expect(samplePatients[i]['Address 1']).toEqual(insertedPatients[i].address1);
      expect(samplePatients[i]['Address 2']).toEqual(insertedPatients[i].address2);
      expect(samplePatients[i].City).toEqual(insertedPatients[i].city);
      expect(samplePatients[i].State).toEqual(insertedPatients[i].state);
      expect(samplePatients[i]['Zip code']).toEqual(insertedPatients[i].zipCode);
      expect(samplePatients[i]['Telephone number']).toEqual(insertedPatients[i].telephoneNumber);
      expect(samplePatients[i]['Email Address']).toEqual(insertedPatients[i].emailAddress);
      expect(samplePatients[i].CONSENT).toEqual(insertedPatients[i].consent);
      expect(samplePatients[i]['Mobile Phone']).toEqual(insertedPatients[i].mobilePhone);
    }
  });

  it('should prepare emails collection correctly', async () => {
    const findPatients = indexModule.__get__('findPatientsForEmailScheduling');
    const prepareEmails = indexModule.__get__('createEmailCollection');
    preparedEmails = prepareEmails(findPatients(patientData));
    expect(preparedEmails.length).toEqual(28); // 7 patients that satify the criteria=>7*4 emails=28
    for (let i = 0; i < preparedEmails.length; i += 4) {
      const firstDate = new Date(preparedEmails[i].scheduled_time);
      let nextDate = new Date(firstDate);
      nextDate = nextDate.setDate(firstDate.getDate() + 1);
      expect(preparedEmails[i].Name).toEqual('Day 1');
      expect(preparedEmails[i + 1].Name).toEqual('Day 2');
      expect(preparedEmails[i + 2].Name).toEqual('Day 3');
      expect(preparedEmails[i + 3].Name).toEqual('Day 4');
      expect(preparedEmails[i + 1].scheduled_time).toEqual(nextDate);
      nextDate = new Date(firstDate).setDate(firstDate.getDate() + 2);
      expect(preparedEmails[i + 2].scheduled_time).toEqual(nextDate);
      nextDate = new Date(firstDate).setDate(firstDate.getDate() + 3);
      expect(preparedEmails[i + 3].scheduled_time).toEqual(nextDate);
    }
  });

  it('should insert emails correctly', async () => {
    const emails = db.collection('Emails');
    const insertDataInMongo = indexModule.__get__('insertDataInMongo');
    await insertDataInMongo(emails, preparedEmails);
    const insertedEmails = await emails.find().toArray(); // should fetch all emails
    expect(insertedEmails.length).toEqual(28);
    for (let i = 0; i < insertedEmails.length; i += 4) {
      const firstDate = new Date(insertedEmails[i].scheduled_time);
      let nextDate = new Date(firstDate);
      nextDate = nextDate.setDate(firstDate.getDate() + 1);
      expect(insertedEmails[i].Name).toEqual('Day 1');
      expect(insertedEmails[i + 1].Name).toEqual('Day 2');
      expect(insertedEmails[i + 2].Name).toEqual('Day 3');
      expect(insertedEmails[i + 3].Name).toEqual('Day 4');
      expect(insertedEmails[i + 1].scheduled_time).toEqual(nextDate);
      nextDate = new Date(firstDate).setDate(firstDate.getDate() + 2);
      expect(insertedEmails[i + 2].scheduled_time).toEqual(nextDate);
      nextDate = new Date(firstDate).setDate(firstDate.getDate() + 3);
      expect(insertedEmails[i + 3].scheduled_time).toEqual(nextDate);
    }
  });

  it('should verify and print PatientIDs where first name is missing', async () => {
    const patients = db.collection('Patients');
    const patientsWithoutFirstName = await patients.find({ firstName: '' }).toArray();
    console.log('Patinets without first name:');
    console.log(patientsWithoutFirstName); // as required in task
    expect(patientsWithoutFirstName.length).toEqual(2); // there are 2 such elements in sample data
  });

  it('should verify and print PatientIDs where email address and missing while CONSENT is Y', async () => {
    const patients = db.collection('Patients');
    const patientsWithoutEmail = await patients.find({ $and: [{ emailAddress: '' }, { consent: 'Y' }] }).toArray();
    console.log('Patinets without email but consent Y:');
    console.log(patientsWithoutEmail);
    expect(patientsWithoutEmail.length).toEqual(1); // there is only one such element
  });
});
